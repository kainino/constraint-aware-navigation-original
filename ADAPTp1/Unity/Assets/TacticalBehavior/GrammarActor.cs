#define ADSTAR

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GrammarActor : MonoBehaviour
{
    public GraphGenerator graphgen;
    public int searchMaxNodes = 1000;
    public float inflation = 2.5f;
    public float maxTimePerUpdate = 0.01f;
    public float multiplierBase = 5f;
    public MeshFilter multField;

    public Behavior subj;
    public TacBeh.Adverb mode;
    public TacBeh.Verb action;
    public TacBeh.Constraint[] constraints;
    public TacBeh.Goal[] goals;
    public bool paused;

    bool doneForever = false;

    public NavigationQueue queue;
    TreeSharpPlus.Node tree = null;

    void Update()
    {
        planner.VisualizeField(multField);
        if (Input.GetKeyDown(KeyCode.Return)) {
            paused = !paused;
        }
        if (queue != null) {
            queue.Ready = !paused && planner.HasValidPlan && !WalkToSelection.AgentsWalking;
        }

        //queue.Ready = Input.GetKey(KeyCode.Space);

        if (Input.GetKeyDown(KeyCode.A)) {
#if true // true = constant replanning
            queue = new NavigationQueue(true);
            queue.OnUpdate += delegate {
                if (doneForever) {
                    return;
                }
                UpdateState();
                if (planner.DonePlanning) {
                    return;
                }
                UpdatePlan(false);
            };
            if (tree == null || !tree.IsRunning) {
                // if we need a new tree, make and run it; otherwise, we
                // only need to update the queue (above). - KN
                tree = PathNav.BuildTree(planner.Beh, queue);
                BehaviorEvent.Run(tree, new[] { planner.Beh.Subj });
            }
#else
            UpdateState();
            UpdatePlan(false);
#endif
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKey(KeyCode.D)) {
            UpdateState();
            UpdatePlan(true);
        }

        PlannerUpdate();
    }

    void UpdateState() {
        if (planner.Beh.Subj == null) {
            return;
        }
        var gr = graphgen.Graph;
        var goals = planner.Beh.Goals;
        if (goals.Count == 0) {
            paused = true;
            return;
        }

        // If we have reached a goal, dequeue it.
        while (goals.Peek().Annotation.MinDistance(planner.Beh.Subj.transform.position) < NavigationQueue.ContinueDistance) {
            goals.Dequeue();
        }

        if (goals.Count == 0) {
            paused = true;
            return;
        }
        var goalannot = goals.Peek().Annotation;

        if (!planner.DonePlanning) {
#if ADSTAR
            //var ss = new ColliderState(planner.Beh.SubjAnnotation);
            var ss = gr.NearestNode(planner.Beh.Subj.transform.position);
            var gs = gr.NearestNode(goalannot.transform.position);
    
            if (startState != null && ss != startState) {
                planner.goalMoved = true;
            }
            if (goalState != null && gs != goalState) {
                // DO NOT USE planner.moved! Use UpdateAfterObstacleMoved, but not
                // UpdateAfterStartMoved. -KN (ref: Francisco)
                planner.restartPlanner(); // doesn't seem to work
                //Start(); // doesn't work either
            }
#else
            var ss = gr.NearestNode(planner.Beh.Subj.transform.position);
            var gs = new ColliderState(goalannot);//, gr.NearestNodes(goalannot));
#endif
            startState = ss;
            goalState = gs;
        }

        if (!planner.firstTime && !WalkToSelection.AgentsWalking) {
            foreach (var c in planner.Beh.Constrs) {
                IEnumerable<DefaultState> changed;
                c.FindNodesToUpdate(gr, out changed);
                planner.UpdateAfterInfluenceMoved(changed);
            }
        }
    }

    void UpdatePlan(bool onestep) {
        planner.OneStep = onestep;
        if (planner.Beh.Subj == null) {
            return;
        }

        var timer = new System.Diagnostics.Stopwatch();
        timer.Start();
#if ADSTAR
        var complete = planner.computePlan(ref goalState, ref startState,
            outputPlan, maxTimePerUpdate);
#else
        var complete = planner.computePlan(ref startState, ref goalState,
            outputPlan, maxTimePerUpdate);
#endif
        timer.Stop();

        var text = string.Format(
            "{0} ms\n{1} visited\n{2} in plan\n{3}",
            timer.ElapsedMilliseconds,
            planner.Visited.dictionary.Count,
            outputPlan.Count, complete);
        if (guiText != null) {
            guiText.text = text;
        }
        //Debug.Log(text);

        queue.NewPlan((planner.HasValidPlan && !paused), outputPlan, goalState);
    }

    #region PLANNERY STUFF
    public ICollection<DefaultState> outputPlan = new List<DefaultState>();
#if ADSTAR
    NavGraphPlannerADStar planner = new NavGraphPlannerADStar();
#else
    NavGraphPlanner planner = new NavGraphPlanner();
#endif

    DefaultState previousState, currentState;
    public bool showOpen = true, showClosed = true,
        showVisited = true, showIncons = true;
    DefaultState startState, goalState;

    void Start()
    {
        var domainList = new List<PlanningDomainBase>();
        domainList.Add(new NavGraphDomain());

        planner.init(ref domainList, searchMaxNodes);

        planner.Beh = new TacBeh() {
            MultiplierBase = multiplierBase,
            Subj = subj,
            Action = action, Mode = mode,
            Constrs = constraints,
            Goals = new Queue<TacBeh.Goal>(goals),
        };
        Debug.Log(planner.Beh);
    }

    void PlannerUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Z))
            showOpen = !showOpen;
        if (Input.GetKeyDown(KeyCode.X))
            showVisited = !showVisited;
        if (Input.GetKeyDown(KeyCode.C))
            showClosed = !showClosed;
        if (Input.GetKeyDown(KeyCode.V))
            showIncons = !showIncons;
    }
 
    void OnDrawGizmos()
    {
        if (planner == null)
            return;
        if (showClosed)
            planner.VisualizeContainer(ContainerType.Close, Color.red, .20f);
        if (showVisited)
            planner.VisualizeContainer(ContainerType.Visited, Color.magenta, .20f);
        if (showOpen)
            planner.VisualizeContainer(ContainerType.Open, Color.green, .20f);
        if (showIncons)
            planner.VisualizeContainer(ContainerType.Incons, Color.yellow, .20f);
        if (planner.HasValidPlan)
            planner.VisualizeContainer(ContainerType.Plan, Color.blue, .20f);
}
    #endregion
}
