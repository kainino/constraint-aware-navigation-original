using UnityEngine;
using System.Collections;

public class CarMove : MonoBehaviour
{
    AnimationCurve curveX;
    public float startT, endT;
    public Transform startP, endP;

    void Start()
    {
        curveX = new AnimationCurve();
        curveX.AddKey(startT, startP.position.x);
        curveX.AddKey(endT, endP.position.x);
 
    }
 
    void Update()
    {
        var p = transform.position;
        transform.position = new Vector3(curveX.Evaluate(Time.time), p.y, p.z);
    }
}
