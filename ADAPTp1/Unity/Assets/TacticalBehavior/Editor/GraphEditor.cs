using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects, CustomEditor(typeof(GraphGeneratorTriangulate))]
public class GraphEditor : Editor {
    void OnSceneGUI()
    {
        var gg = target as GraphGenerator;
        if (gg == null) return;

        var graph = (target as GraphGenerator).Graph;
        if (graph == null) return;

        foreach (var n in graph.Nodes) {
            foreach (var a in n.Adjacencies) {
                Handles.DrawLine(n.Position, a.Position);
            }
            var newpos = Handles.FreeMoveHandle(
                n.Position, Quaternion.identity,
                0.2f, Vector3.zero, Handles.DotCap);
            newpos.y = n.Position.y;
            n.Position = newpos;
        }
    }
}
