using UnityEngine;
using System.Collections;

public class NavGraphGridNode : NavGraphNode
{
    public NavGraphGridNode(Vector3 pos)
    {
        Position = pos;
    }
}
