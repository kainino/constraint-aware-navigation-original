using UnityEngine;
using System.Collections;
using UnityEditor;

public abstract class GraphGenerator : MonoBehaviour
{
    public NavGraph Graph { get; protected set; }

    void OnDrawGizmos()
    {
        if (Graph == null || Graph.Nodes == null) {
            return;
        }
        foreach (var n in Graph.Nodes) {
            Gizmos.color = new Color(0, 0, 1, 0.4f);
            foreach (var a in n.Adjacencies) {
                Gizmos.DrawLine(n.Position, a.Position);
            }
            //Gizmos.DrawCube(n.Position, new Vector3(0.3f, 0.3f, 0.3f));
        }
    }
}
