using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// A node in the NavGraph structure at an edge midpoint
/// </summary>
public class EdgeNode : NavGraphNode
{
    public EdgeNode(NavMeshEdge e)
    {
        Position = e.GetMidpoint();
    }
}
