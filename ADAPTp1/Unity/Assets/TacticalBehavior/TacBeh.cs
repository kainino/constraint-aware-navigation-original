using UnityEngine;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// SUBJ, ACTION MODE [CONSTR CONSTROBJ] GOAL GOALOBJ
/// </summary>
[System.Serializable]
public partial struct TacBeh
{
    public float MultiplierBase;

    public Behavior Subj { get; set; }

    public TacBeh.Verb Action { get; set; }

    public TacBeh.Adverb Mode { get; set; }

    public TacBeh.Constraint[] Constrs { get; set; }

    public Queue<TacBeh.Goal> Goals { get; set; }

    public override string ToString()
    {
        return string.Format(
            "[TacBeh: {0}, {1} {2} {3} {4}]",
            Subj.name, Action,
            Mode == TacBeh.Adverb.None ? "" : Mode.ToString(),
            "(" + string.Join(", ", Constrs.Select(c => c.ToString()).ToArray()) + ")",
            "(" + string.Join(", ", Goals.Select(g => g.ToString()).ToArray()) + ")");
    }

    public BoxCollider SubjAnnotation
    {
        get {
            var o = Subj.GetComponent<Annotations>();
            if (o == null) return Subj.collider as BoxCollider;
            return o.Near;
        }
    }

    public float Speed
    {
        get {
            if (Mode == TacBeh.Adverb.Quickly) return 5.7f; // running
            else if (Mode == TacBeh.Adverb.Slowly) return 1f; // walking
            else return 2.2f;
        }
    }

    public float MultiplierField(Vector3 mp)
    {
        // This is where we change the base multiplier
        // (i.e. the multiplier when there are no constraints).
        float ret = MultiplierBase;
        if (Constrs != null) {
            foreach (var c in Constrs) {
                ret += c.MultiplierField(mp);
            }
        }
        return Mathf.Max(1f, ret); //Mathf.Max(1f, ret);
    }
}
