using UnityEngine;
using System.Collections;

public class ScreenshotCapture : MonoBehaviour {
	
	public int currentScreenshotIndex;
	
	public string screenshotPrefix = "screen"; 
	public string recordingDirectory = "Screenshots"; // this folder must exist 
	
	public int captureFrameRate; // the suggested frame rate for movie capture 
	public bool recordMovie; // if this is set to true, it dumps frames at the suggested frame rate 
	
	private int _currentScreenshotIndex;
	
	// Use this for initialization
	void Start () {
	
		Time.captureFramerate = captureFrameRate;
	}
	
	void FixedUpdate () {
	
		
		if ( recordMovie ) 
		{
			captureScreenshot ();
		}
		else if ( Input.GetKeyDown(KeyCode.X) )
		{
			captureScreenshot ();
		}
	}
	
	private void captureScreenshot () 
	{
		string newScreenshotName = generateScreenShotName();
		Application.CaptureScreenshot(newScreenshotName);
		_currentScreenshotIndex ++;
		
		currentScreenshotIndex = _currentScreenshotIndex;
		
	}
	
	private string generateScreenShotName ()
	{
		int numberOfZeroes = 5;
	
		int t  = _currentScreenshotIndex; 
		int numberOfDigits = 0;
		
		while (t != 0)
		{
			t = t / 10;
			numberOfDigits ++;
		}
	
		numberOfZeroes -= numberOfDigits;
		
		string newScreenshotName = screenshotPrefix;
		
		for ( int i = 0; i < numberOfZeroes; i ++) 
			newScreenshotName = newScreenshotName + "0";
			
		if ( _currentScreenshotIndex != 0 )
			newScreenshotName = recordingDirectory + "/" + newScreenshotName + _currentScreenshotIndex + ".png";
		else
			newScreenshotName = recordingDirectory + "/" + newScreenshotName + ".png";
		
		return newScreenshotName;
	}
}